function MessageQueue() {
    let messages = [];
    let subscribers = [];
    let sending = false;
    this.publish = function (data, unique) {
        if (unique && messages.indexOf(data) < 0) messages.push(data);
        if (!sending) {
            sending = true;
            setTimeout(function () {
                while (messages.length > 0) {
                    for (let j = 0; j < subscribers.length; j++) {
                        subscribers[j](messages.shift());
                    }
                }
                messages = [];
                sending = false;
            });
        }
    }
    this.subscribe = function (fn) {
        subscribers.push(fn);
    }
    this.unsubscribe = function (fn) {
        subscribers = subscribers.filter(s => s !== fn);
    }
}

const DOM = {
    setAttribute: function (el, k, v) {
        el.setAttribute(k, v);
    },
    createTextNode: function (txt) {
        return document.createTextNode(txt);
    },
    setValue: function (el, v) {
        el.value = v;
    },
    removeAttribute: function (el, k) {
        el.removeAttribute(k);
    },
    setText: function (el, text) {
        el.textContent = text;
    },
    insertBefore: function (childDom, newChildDom) {
        childDom.parentElement.insertBefore(newChildDom, childDom);
    },
    replaceWith: function (oldDomNode, newDom) {
        if (Array.isArray(newDom)) {
            oldDomNode.replaceWith(...newDom);
        } else {
            oldDomNode.replaceWith(newDom);
        }
    },
    append: function (parent, children) {
        if (!children) return;
        if (Array.isArray(children)) {
            children.length > 0 && parent.append(...children);
        } else {
            parent.append(children)
        }
    },
    createElement: function (tag) {
        return document.createElement(tag);
    },
    removeEventListener: function (el, ev, fn) {
        el.removeEventListener(ev, fn);
    },
    addEventListener: function (el, ev, fn) {
        el.addEventListener(ev, fn);
    },
    remove: function (el) {
        el.remove();
    }
};

const componentUpdatesQueue = new MessageQueue();

const _T_TEXT = 1;
const _T_ELEMENT = 2;
const _T_FRAGMENT = 3;
const _T_COMPONENT = 4;

/**
 * VNode interface
 * @interface
 * @type {{domNode: any, nodes: any, _type: number, parent: VNode}}
 */
const VNode = {};

/**
 * vtext node
 * @param {string} text
 * @implements {VNode}
 */
function VText(text) {
    this._type = _T_TEXT;
    this.text = text;
    this.domNode = undefined;
    this.parent = undefined;
    this.unmount = function unmount() {
        DOM.remove(this.domNode);
        return this.domNode;
    }

    this.remove = function remove() {
        return this.unmount();
    }

    this.firstDomNode = function firstDomNode() {
        return this.domNode;
    }

    this.parentDomNode = function parentDomNode() {
        return this.parent.domNode || this.parent.parentDomNode();
    }

    this.toString = function () {
        return `VText{text:${JSON.stringify(this.text)}, parent:(${this.parent && this.parent.toString()})}`;
    }
}

/**
 * normalize nodes
 * @param {any} nodes
 * @param {VNode} parent
 * @returns {*[]}
 */
function normalizeNodes(nodes, parent) {
    nodes = nodes || [];
    if (!Array.isArray(nodes)) nodes = [nodes];

    if (nodes.length === 0) return nodes;

    const norm = [];
    for (let i = 0; i < nodes.length; i++) {
        let node = nodes[i];

        if (Array.isArray(node)) {
            node = new VFragment(node);
        }
        if (!node || !node._type) {
            node = new VText(node);
        }
        node.parent = parent;
        norm.push(node);
    }

    return norm;
}

/**
 * vfragment node
 * @param {[]} nodes
 * @implements {VNode}
 */
function VFragment(nodes) {
    this._type = _T_FRAGMENT;
    this.nodes = normalizeNodes(nodes, this);
    this.parent = undefined;

    this.remove = function remove() {
        if (!this.nodes) return;
        for (const node of this.nodes) {
            node.remove();
        }
    }

    this.firstDomNode = function firstDomNode() {
        let domNode = undefined;
        for (const node of this.nodes) {
            domNode = node.domNode || node.firstDomNode();
            if (domNode !== undefined) break;
        }
        return domNode;
    }

    this.parentDomNode = function parentDomNode() {
        return this.parent.domNode || this.parent.parentDomNode();
    }

    this.toString = function () {
        return `VFragment{nodes:${this.nodes && this.nodes.length}, parent:(${this.parent && this.parent.toString()})}`;
    }
}

/**
 * velement node
 * @param {string} tag
 * @param {[]} nodes
 * @param {{}} [attrs]
 * @param {{}} [listeners]
 * @implements {VNode}
 */
function VElement(tag, nodes, attrs, listeners) {
    this._type = _T_ELEMENT;
    this.tag = tag;
    this.nodes = normalizeNodes(nodes, this);
    this.parent = undefined;
    this.domNode = undefined;
    this.attrs = attrs;
    this.listeners = [];
    if (Array.isArray(listeners)) {
        this.listeners = listeners;
    } else {
        listeners = listeners || {};
        for (const event in listeners) {
            this.listeners.push([event, listeners[event]]);
        }
    }

    this.unmount = function unmount() {
        // this.domNode.remove();
        DOM.remove(this.domNode);
        return this.domNode;
    }

    this.remove = function () {
        this.unmount();
    }

    this.firstDomNode = function firstDomNode() {
        return this.domNode;
    }

    this.parentDomNode = function parentDomNode() {
        return this.domNode;
    }

    this.toString = function () {
        return `VElement{tag:${this.tag}, nodes:${this.nodes && this.nodes.length}, parent:(${this.parent && this.parent.toString()})`
            + `, attrs:${JSON.stringify(this.attrs)}, listeners:[${this.listeners && JSON.stringify(listeners.map(l => l[0]))}]`;
    }
}

const _components = [];

function curComponent() {
    return _components[_components.length - 1];
}

const _updateLocked = {
    value: false
};

const defaultProto = {__CTOR__: true, props: {}, state: {}, methods: []};

function createProto(ctor) {
    let val;
    let methods = [];
    let prop = '';
    const proto = Object.assign({}, defaultProto);
    for (prop in ctor) {
        val = ctor[prop];
        if (typeof val === "function" && prop !== 'render') {
            methods.push([prop, val]);
        } else {
            proto[prop] = val;
        }
    }
    proto.methods = methods;

    return proto;
}

/**
 * component
 * @param {{[__CTOR__], name: string, render: function, [state]: {}, [props]: {}, [methods]: {}, [onCreate]: function, [onMount]: function, [onUpdate]: function, [onDestroy]: function}} ctor
 * @implements {VNode}
 */
function Component(ctor) {
    this._type = _T_COMPONENT;

    this.parent = undefined;
    this._name = ctor.name;
    Object.defineProperty(this, '_proto', {value: ctor.__CTOR__ ? ctor : Object.freeze(createProto(ctor))})
    this.nodes = undefined;
    this.state = ctor.state || {};
    this.props = ctor.props || {};
    this.children = [];
    this.$__INTERNAL = {
        created: false,
        mounted: false
    }

    this.onCreate = this.onUpdate = this.onMount = function () {
        return true;
    };

    this._setProps = function (obj) {
        this.props = Object.assign({}, this.props, obj);
    }

    this.setChildren = function (ch) {
        this.children = ch || [];
    }

    this.setState = function (obj, dontUpdate) {
        dontUpdate = _updateLocked.value || dontUpdate;
        this.state = this.state || {};
        obj = obj || {};
        const newState = Object.assign({}, this.state, obj);
        const shouldUpdate = this.shouldUpdate ? this.shouldUpdate(this.props, newState) : true;
        this.state = newState;
        if ((!dontUpdate) && shouldUpdate) {
            this.update();
        }
    }

    this.setNodes = function (arr) {
        this.nodes = arr;
    }

    this.updateNow = function () {
        try {
            _components.push(this);
            const oldNodes = this.nodes;
            this.renderAndSetNodes();
            patchDom_childNodes(this.nodes, oldNodes, this.parent, this.parentDomNode());
        } catch (e) {
            console.error(e)
        } finally {
            _components.pop();
        }
    }

    this.update = function () {
        const _component = this;
        componentUpdatesQueue.publish(_component, true);
    }

    /**
     * @param [props]
     * @param [state]
     * @return {VNode[]}
     */
    this.invokeRender = function (props, state) {
        return normalizeNodes(this._proto.render.call(this, props || this.props, state || this.state), this);
    }

    function setBoundMethods(instance) {
        let method;
        for (method of instance._proto.methods) {
            instance[method[0]] = method[1].bind(instance);
        }
    }

    this.renderAndSetNodes = function (props, state) {
        setBoundMethods(this);
        _updateLocked.value = true;
        if (!this.$__INTERNAL.created) {
            if (this.onCreate)
                try {
                    this.onCreate()
                } catch (e) {
                    console.error(e);
                }
            this.$__INTERNAL.created = true;
        } else {
            if (this.onUpdate)
                try {
                    this.onUpdate();
                } catch (e) {
                    console.error(e);
                }
        }
        _updateLocked.value = false;
        this.setNodes(this.invokeRender(props, state));
    }

    /**
     *
     * @param props
     * @param children
     * @return {VNode}
     */
    this.createInstance = function (props, children) {
        props = props || {};
        const inst = new Component(this._proto);
        inst.setChildren(children);
        inst._setProps(props);
        return inst;
    }

    this.remove = function remove() {
        if (!this.nodes) return;
        for (const node of this.nodes) {
            node.remove();
        }
    }

    this.firstDomNode = function firstDomNode() {
        let domNode = undefined;
        for (const node of this.nodes) {
            domNode = node.domNode || node.firstDomNode();
            if (domNode !== undefined) break;
        }
        return domNode;
    }

    this.parentDomNode = function parentDomNode() {
        return this.parent && (this.parent.domNode || this.parent.parentDomNode());
    }
}

function shouldComponentUpdate(component, newProps, newState) {
    return component.shouldUpdate ? component.shouldUpdate(newProps, newState) : true;
}

// Node collections
Component.prototype.unmount = VFragment.prototype.unmount = function unmount() {
    if (!this.nodes) return;
    let domNode = undefined;
    for (const node of this.nodes) {
        if (!domNode) domNode = node.unmount();
        else node.unmount();
    }
    return domNode;
}

// Node types with child nodes
Component.prototype.firstDomNodeAfter = VFragment.prototype.firstDomNodeAfter = VElement.prototype.firstDomNodeAfter =
    function firstDomNodeAfter(child) {
        const start = this.nodes.indexOf(child) + 1;
        let domNode = undefined;

        // find next dom in child nodes
        for (let i = start; i < this.nodes.length && !domNode; i++)
            domNode = this.nodes[i].firstDomNode();

        // if node is a collection, try to find in parent
        if (!domNode && !this.domNode) {
            domNode = this.nextDomSibling();
        }

        return domNode;
    };

// Node types with child nodes
Component.prototype.nextDomSibling = VFragment.prototype.nextDomSibling = VElement.prototype.nextDomSibling =
    function nextDomSibling() {
        return this.parent && this.parent.firstDomNodeAfter(this);
    };

VText.prototype.firstDomNodeAfter =
    function firstDomNodeAfter(child) {
        console.error("!!! VText is parent of", child)
        return undefined;
    };

function renderVText(vElement) {
    let textNode = DOM.createTextNode(vElement.text);
    vElement.domNode = textNode;
    return textNode;
}

function setElementAttribute(domNode, key, value) {
    switch (key) {
        case 'value':
            DOM.setValue(domNode, value);
            break;
        default:
            DOM.setAttribute(domNode, key, value);
    }
}

function setVElementAttributes(vElement) {
    if (!vElement.attrs) return;

    for (const attrsKey in vElement.attrs) {
        setElementAttribute(vElement.domNode, attrsKey, vElement.attrs[attrsKey]);
    }
}

function patchVElementAttributes(newEl, oldEl) {
    const oldAttrs = oldEl.attrs || {};
    const newAttrs = newEl.attrs || {};
    const newKeys = [];
    for (const key in newAttrs) {
        newKeys.push(key);
        const newVal = newAttrs[key];
        const oldVal = oldAttrs[key];
        if (!(oldVal && newVal === oldVal)) {
            setElementAttribute(newEl.domNode, key, newVal);
        }
    }
    for (const key in oldAttrs) {
        if (!key in newKeys) {
            DOM.removeAttribute(newEl.domNode, key);
        }
    }
}

function addVElementEventListeners(vElement) {
    const cmp = curComponent() || {};
    if ((!vElement.listeners) || !vElement.listeners.length > 0) return;
    let event;
    let wrapper;
    for (let i = 0; i < vElement.listeners.length; i++) {
        event = vElement.listeners[i];

        const fn = event[1];
        wrapper = function (ev) {
            return fn.call(cmp, ev, vElement.domNode);
        }
        event[1] = wrapper;

        DOM.addEventListener(vElement.domNode, event[0], wrapper);
    }
}

// TODO merge with patch
function removeVElementEventListeners(vElement) {
    if (!vElement.listeners) return;
    for (const listener of vElement.listeners) {
        DOM.removeEventListener(vElement.domNode, listener[0], listener[1]);
    }
    vElement.listeners = [];
}

// TODO optimize
function patchVElementEventListeners(newEl, oldEl) {
    removeVElementEventListeners(oldEl);
    addVElementEventListeners(newEl);
}


function renderVElement(vElement) {
    vElement.domNode = DOM.createElement(vElement.tag);

    setVElementAttributes(vElement);
    addVElementEventListeners(vElement);

    if (Array.isArray(vElement.nodes) && vElement.nodes.length > 0) {

        for (let node of vElement.nodes) {
            let domNode = renderVNode(node);
            DOM.append(vElement.domNode, domNode);
        }
    }

    return vElement.domNode;
}

function _renderCollection(vnode) {
    if (!Array.isArray(vnode.nodes)) {
        return [];
    }

    const domNodes = [];
    for (let node of vnode.nodes) {

        let domNode = renderVNode(node);

        if (!Array.isArray(domNode)) {
            domNodes.push(domNode);
        } else {
            domNodes.push(...domNode);
        }

    }
    return domNodes;
}

function renderVFragment(frag) {
    return _renderCollection(frag)
}

function renderComponent(component) {
    _components.push(component);
    if (!component.nodes) {
        component.renderAndSetNodes();
    }
    const dom = _renderCollection(component);
    _components.pop();
    return dom;
}

/**
 * render vnode
 * @returns {any|any[]}
 */
function renderVNode(vnode) {
    switch (vnode._type) {
        case _T_TEXT:
            return renderVText(vnode);
        case _T_ELEMENT:
            return renderVElement(vnode);
        case _T_FRAGMENT:
            return renderVFragment(vnode);
        case _T_COMPONENT:
            return renderComponent(vnode);
        default:
            console.warn(`VNode type is not valid: ${vnode._type}`)
            return []
    }
}

function patchDom_replace(newNode, oldNode, rootDom) {
    let oldDomNode = oldNode.domNode || oldNode.firstDomNode();

    const newDom = renderVNode(newNode);

    //DOM
    if (oldDomNode) {
        DOM.replaceWith(oldDomNode, newDom);
    } else {
        oldDomNode = oldNode.nextDomSibling();
        if (oldDomNode) {
            let placeHolder = DOM.createElement('script');
            DOM.insertBefore(oldDomNode, placeHolder);
            DOM.replaceWith(placeHolder, newDom);
        } else {
            if (!rootDom) throw Error(`No parent dom found! oldNode: ${oldNode}, newNode: ${newNode}`);

            DOM.append(rootDom, newDom);

        }
    }
    oldNode.unmount();
}

function patchDom_addExtraNodes(extraDomNodes, parentVNode, rootDom) {
    if (parentVNode && parentVNode.domNode) {
        // parentVNode.domNode.append(...extraDomNodes);
        DOM.append(parentVNode.domNode, extraDomNodes);
    } else {
        let nextDom = (parentVNode && parentVNode.nextDomSibling(parentVNode));
        if (nextDom) {
            extraDomNodes = extraDomNodes.reverse();
            // const parentDomNode = nextDom.parentElement;
            while (extraDomNodes.length > 0) {
                // parentDomNode.insertBefore(extraDomNodes.pop(), nextDom);
                DOM.insertBefore(nextDom, extraDomNodes.pop());
            }
        } else {
            if (rootDom) {
                // rootDom.append(...extraDomNodes);
                DOM.append(rootDom, extraDomNodes);
            } else {
                throw Error(`No parent dom found! parentNode: ${parentVNode}, nextDom: ${nextDom}, rootDom: ${rootDom}`);
            }
        }
    }
}

function patchDom_childNodes(newNodes, oldNodes, parentVNode, rootDom) {
    newNodes = newNodes || [];
    oldNodes = oldNodes || [];
    newNodes = Array.isArray(newNodes) ? newNodes : [newNodes];
    oldNodes = Array.isArray(oldNodes) ? oldNodes : [oldNodes];

    const newCount = newNodes.length, oldCount = oldNodes.length;

    let idx = 0;
    while (idx < oldCount && idx < newCount) {
        const newNode = newNodes[idx];
        const oldNode = oldNodes[idx];

        const sameType = newNode._type === oldNode._type;

        if (!sameType) {
            patchDom_replace(newNode, oldNode, rootDom);
            idx++;
            continue;
        }

        const newType = newNode._type;

        switch (newType) {
            case _T_TEXT: {
                patchDom_VText(newNode, oldNode);
                break;
            }
            case _T_ELEMENT: {
                if (newNode.tag === oldNode.tag) {
                    patchDom_VElement(newNode, oldNode);
                } else {
                    patchDom_replace(newNode, oldNode, rootDom);
                }
                break;
            }
            case _T_FRAGMENT: {
                patchDom_VFragment(newNode, oldNode, parentVNode, rootDom);
                // patchDom_childNodes(newNode.nodes, oldNode.nodes, oldNode, rootDom);
                break;
            }
            case _T_COMPONENT: {
                if (newNode._proto === oldNode._proto) {
                    newNodes[idx] = patchDom_Component(newNode, oldNode, parentVNode, rootDom);
                } else {
                    patchDom_replace(newNode, oldNode, rootDom);
                }
                break;
            }
            default:
                throw Error(`Cannot patch node type ${newType}`);
        }
        idx++;
    }

    // remove old nodes
    if (newCount < oldCount) {
        //DOM
        for (let i = newCount; i < oldCount; i++) {
            oldNodes[i].unmount();
        }
    }

    // append new nodes
    if (newCount > oldCount) {
        let extraDomNodes = [];
        for (let i = oldCount; i < newCount; i++) {
            let dom = renderVNode(newNodes[i]);
            if (Array.isArray(dom)) {
                extraDomNodes.push(...dom);
            } else {
                extraDomNodes.push(dom);
            }
        }
        patchDom_addExtraNodes(extraDomNodes, parentVNode, rootDom);
    }
}

function patchDom_VElement(newEl, oldEl) {
    newEl.domNode = oldEl.domNode;

    // setVElementAttributes(newEl);
    patchVElementAttributes(newEl, oldEl);
    // removeVElementEventListeners(oldEl);
    // addVElementEventListeners(newEl);
    patchVElementEventListeners(newEl, oldEl);

    patchDom_childNodes(newEl.nodes, oldEl.nodes, oldEl, newEl.domNode);
}

function patchDom_VText(newTxt, oldTxt) {
    newTxt.domNode = oldTxt.domNode;

    if (oldTxt.text !== newTxt.text) {
        DOM.setText(newTxt.domNode, newTxt.text);
    }
}

function patchDom_VFragment(newFrag, oldFrag, parentVNode, rootDom) {
    patchDom_childNodes(newFrag.nodes, oldFrag.nodes, oldFrag, rootDom)
}

function patchDom_Component(newComponent, oldComponent, parentVNode, rootDom) {
    const shouldUpdate = oldComponent.shouldUpdate ? oldComponent.shouldUpdate(newComponent.props, oldComponent.state) : true;
    if (shouldUpdate) {
        _components.push(newComponent);
        newComponent.$__INTERNAL = oldComponent.$__INTERNAL;
        newComponent.setState(oldComponent.state, true);
        newComponent.renderAndSetNodes();
        patchDom_childNodes(newComponent.nodes, oldComponent.nodes, oldComponent, rootDom);
        _components.pop();
        return newComponent;
    } else {
        oldComponent.parent = parentVNode;
        return oldComponent;
    }
}

function patchDom(newNode, oldNode, parentVNode, rootDom) {
    patchDom_childNodes(
        Array.isArray(newNode) ? newNode : [newNode],
        Array.isArray(oldNode) ? oldNode : [oldNode],
        parentVNode, rootDom);
}

function parseProps(props) {
    let attrs = {}
    let listeners = []
    for (const propKey in props) {
        const val = props[propKey];
        if (propKey.startsWith('on')) {
            listeners.push([propKey.slice(2).toLowerCase(), val])
        } else {
            attrs[propKey] = val;
        }
    }
    return [attrs, listeners];
}

function createVElement(tag, props, children) {
    let [attrs, listeners] = [undefined, undefined];
    if (props !== undefined && props._type === undefined) {
        if (!Array.isArray(props)) {
            [attrs, listeners] = parseProps(props);
        }
        return new VElement(tag.toLowerCase(), Array.isArray(props) ? props : children, attrs, listeners);
    }
}

function createComponent(name, props) {
    if (typeof props === "function") {
        props = {render: props}
    }
    props = props || {};
    return new Component({name, ...props});
}

/**
 * hyperscript
 * @param [p1]
 * @param [p2]
 * @param [p3]
 * @returns {VNode}
 */
function h(p1, p2, p3) {
    if (p1 instanceof Component) {
        if (Array.isArray(p2)) {
            p3 = p2;
            p2 = {};
        }
        return p1.createInstance(p2, p3);
    }
    if (!p1) {
        return new VFragment(Array.isArray(p2) ? p2 : p3);
    }
    if (p1 && typeof p1 === "string") {
        let [attrs, listeners] = [undefined, undefined];
        if (p2 !== undefined && p2._type === undefined) {
            if (!Array.isArray(p2)) {
                [attrs, listeners] = parseProps(p2);
            }
        }
        return new VElement(p1, Array.isArray(p2) ? p2 : p3, attrs, listeners);
    }

    throw Error('Cannot create node ' + JSON.stringify({arguments}));
}

componentUpdatesQueue.subscribe(function (component) {
    if (requestAnimationFrame) {
        requestAnimationFrame(function () {
            component.updateNow();
        })
    } else {
        component.updateNow();
    }
})

/**
 * render node(s) to dom
 * @param {VNode} vnode
 * @param {HTMLElement} [rootDom]
 */
function render(vnode, rootDom) {
    let attrs = {};
    for (const attrName of rootDom.getAttributeNames()) {
        attrs[attrName] = rootDom.getAttribute(attrName);
    }
    vnode = new VElement(rootDom.tagName, [vnode], attrs);
    if (!rootDom['__NODE']) {
        rootDom.innerHTML = "";
        patchDom_childNodes(vnode.nodes, [], vnode, rootDom);
        vnode.domNode = rootDom;
        rootDom['__NODE'] = vnode;
    } else {
        patchDom(vnode, rootDom['__NODE'], vnode, rootDom);
        rootDom['__NODE'] = vnode;
    }
}

function curComponentCount() {
    console.log(_components.length)
}
const dom = {
    setAttribute: function (el, k, v) {
        el.setAttribute(k, v);
    },
    setProperty: function (el, k, v) {
        el[k] = v;
    },
    setValue: function (el, v) {
        el.value = v;
    },
    replaceWith: function (oldDom, newDom) {
        if (Array.isArray(newDom)) {
            oldDom.replaceWith.apply(oldDom, newDom);
        } else {
            oldDom.replaceWith(newDom);
        }
    },
    getElementAttributes: function (el) {
        const attrs = {};
        for (const attrName of el.getAttributeNames()) {
            attrs[attrName] = el.getAttribute(attrName);
        }
        return attrs;
    },
    insertBefore: function (child, newNode) {
        child.parentElement.insertBefore(newNode, child);
    },
    createElement: function (tag) {
        return document.createElement(tag);
    },
    append: function (parent, children) {
        if (!children) return;
        if (Array.isArray(children)) {
            children.length > 0 && parent.append.apply(parent, children);
        } else {
            parent.append(children)
        }
    },
    clear: function (el) {
        el.innerHTML = '';
    },
    createTextNode: function (txt) {
        return document.createTextNode(txt);
    }
};
